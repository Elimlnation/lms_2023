from django.db import models

class Group(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    max_students = models.PositiveIntegerField()
    room_number = models.CharField(max_length=10)